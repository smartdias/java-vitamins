/**
 * 
 */
package com.java.vitamins.utils;

/**
 * @author murli_000
 * Defaults
 *
 */
public interface Defaults {

	Boolean TRUE = new Boolean(true);
	Boolean FALSE = new Boolean(false);
	
	String BLANK = "";
	
	int DEFAULT_FRACTION_DIGITS = 2;
	double PERCENT = 0.01;

	
}
