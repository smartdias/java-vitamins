/**
 * 
 */
package com.java.vitamins.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.CalendarConverter;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.apache.commons.beanutils.converters.LongConverter;
import org.apache.log4j.Logger;

import com.java.vitamins.bo.BaseBO;

/**
 * @author murli_000
 * 
 */
public class TransformationUtil {

	private static final Logger LOGGER = Logger.getLogger(TransformationUtil.class);

	/**
	 * 
	 */
	private TransformationUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @param baseBO
	 * @param obj
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static Object buildEntity(BaseBO baseBO, Object obj) throws IllegalAccessException, InvocationTargetException {
		final BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
		
		beanUtilsBean.getConvertUtils().register(new CalendarConverter( null), Calendar.class);
		beanUtilsBean.getConvertUtils().register(new IntegerConverter( null), Integer.class);
		beanUtilsBean.getConvertUtils().register(new LongConverter(null), Long.class);
		
		beanUtilsBean.copyProperties(obj, baseBO);
		
		return obj;
	}
	
	/**
	 * 
	 * @param obj
	 * @param baseBO
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	public static BaseBO buildBO(Object obj, BaseBO baseBO) throws IllegalAccessException, InvocationTargetException {
		final BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
		
		beanUtilsBean.getConvertUtils().register(new CalendarConverter( null), Calendar.class);
		beanUtilsBean.getConvertUtils().register(new IntegerConverter( null), Integer.class);
		beanUtilsBean.getConvertUtils().register(new LongConverter(null), Long.class);
		
		beanUtilsBean.copyProperties(baseBO, obj);
		
		return baseBO;
	}

}
